# ncurses Minesweeper

## Usage

To compile the game, run :

```
make
```

To install it, use :

```
sudo make install
```

You can then launch it with

```
minesweeper <height> <width> <difficulty (between 1 and 9)>
```


