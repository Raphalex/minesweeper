#ifndef __GRID_H__
#define __GRID_H__


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COLOR_GRAY 235 //Just a definition for the gray color in ncurses

typedef struct cell{ //Cell structure for the grid
	int val;//Value of the cell (-1 indicates a bomb)
	char state;/*Character that represents the state of the cell (revealed,
   	hidden, flag)*/
}cell;

typedef struct s_grid{//Grid structure
	int h;//Height
	int w;//Width
	cell ** grid;//The grid itself (array of arrays)
}*grid;


grid grid_init(int h, int l, float diff);/*Initializes a grid of height 'h'
 and width 'l' with difficulty 'diff' (Difficulty means that there will be 
 approximately (diff*10)% of bombs in the grid*/

grid attr_nbr(grid g);/*Changes the values of cells in the grid according to 
						their surroundings */

void free_grid(grid g);/*Frees the grid g*/

#endif
