#include <ncurses.h>
#include "grid.h"

#define OFFSET 5
#define HEIGHT 3
#define WIDTH 4

void ncurses_init();/*Iniializes ncurses*/

void display_grid(grid g);/*Displays the grid g*/

void game(grid g);/*Main game loop */

void step(grid g,int posY, int posX,int *perdu);/*Function that computes the 
grid again when the player acts upon a cell of the grid */




