#include "grid.h"

grid grid_init(int h, int l, float diff){
	grid new=malloc(sizeof(struct s_grid));
	new->h=h;
	new->w=l;
	new->grid= malloc(h*sizeof(cell));
	for(;h>0;h--){
		new->grid[h-1]=malloc(l*sizeof(cell));
	}
	h=new->h-1;
	l=new->w-1;
	for(;h>=0;h--){
		for(l=new->w-1;l>=0;l--){
			cell c;
			c.state='c';
			c.val=0;
			if((rand() % 100)<(diff*10)){
				c.val=-1;
			}
			else{
				c.val=0;
			}
			new->grid[h][l]=c;
		}
	}
	new = attr_nbr(new);
	return new;
}

grid attr_nbr(grid g){
	int h=g->h-1;
	int l=g->w-1;
	int bombes;
	for(;h>=0;h--){
		for(l=g->w-1;l>=0;l--){
			bombes=0;
			if(g->grid[h][l].val!=-1){
				if(h!=0){
					bombes+=g->grid[h-1][l].val==-1? 1:0;
				}
				if(h!=g->h-1){
					bombes+=g->grid[h+1][l].val==-1? 1:0;
				}
				if(l!=0){
					bombes+=g->grid[h][l-1].val==-1? 1:0;
				}
				if(l!=g->w-1){
				bombes+=g->grid[h][l+1].val==-1? 1:0;
				}
				if(l!=g->w-1 && h!=g->h-1){
					bombes+=g->grid[h+1][l+1].val==-1? 1:0;
				}
				if(l!=0 && h!=g->h-1){
					bombes+=g->grid[h+1][l-1].val==-1? 1:0;
				}
				if(h!=0 && l!=0){
					bombes+=g->grid[h-1][l-1].val==-1? 1:0;
				}
				if(h!=0 && l!=g->w-1){
					bombes+=g->grid[h-1][l+1].val==-1? 1:0;
				}
			g->grid[h][l].val=bombes;
			}
		}
	}
	return g;
}

void free_grid(grid g){
	int h=g->h-1;
	for(;h>=0;h--){
		free(g->grid[h]);
	}
	free(g->grid);
	free(g);
}

			


				

			




