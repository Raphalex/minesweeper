#include "io.h"

void ncurses_init(){
	initscr();
	cbreak();
	noecho();
	keypad(stdscr,TRUE);	
}

void display_grid(grid g){
	for(int h=0;h<g->h;h++){
		curs_set(0);
		for(int l=0;l<=g->w;l++){
			mvaddch(h*HEIGHT+OFFSET,l*WIDTH+OFFSET,'|');
			mvaddch(h*HEIGHT+OFFSET-1,l*WIDTH+OFFSET,'|');
			mvaddch(h*HEIGHT+OFFSET+1,l*WIDTH+OFFSET,'|');
		}
	}
	for(int h=0;h<=g->h;h++){
		for(int l=0;l<g->w;l++){
			mvaddch(h*HEIGHT+OFFSET-2,l*WIDTH+OFFSET+1,'_');
			mvaddch(h*HEIGHT+OFFSET-2,l*WIDTH+OFFSET+2,'_');
			mvaddch(h*HEIGHT+OFFSET-2,l*WIDTH+OFFSET+3,'_');
		}
	}
}

void colors_init(){
		init_pair(1,COLOR_WHITE,COLOR_BLACK);
		init_pair(2,COLOR_RED,COLOR_BLACK);
		init_pair(4,COLOR_BLUE,COLOR_BLACK);
		init_pair(5,COLOR_GREEN,COLOR_BLACK);
		init_pair(6,COLOR_MAGENTA,COLOR_BLACK);
		init_pair(7,COLOR_YELLOW,COLOR_BLACK);
		init_pair(8,COLOR_RED,COLOR_BLACK);
		init_pair(9,COLOR_RED,COLOR_BLACK);
		init_pair(10,COLOR_RED,COLOR_BLACK);
		init_pair(11,COLOR_RED,COLOR_BLACK);
		init_pair(12,COLOR_WHITE,235);
		init_pair(13,COLOR_WHITE,COLOR_BLACK);
		init_pair(14,202,COLOR_GRAY);
		init_pair(15,COLOR_RED,COLOR_BLACK);
}
void random_hit(grid g){
	int end=0;
	int lost=0;
	while(!end){
				for(int i=0;i<g->h && !end;i++){
					for(int j=0;j<g->w && !end;j++){
						int lol;
						srand(time(NULL));
						lol=rand()%100;
						if(lol<80 && g->grid[i][j].val==0){
							step(g,i,j,&lost);
							end=1;
						}
					}
				}
			}
}

void color_default(int y, int x){
	for(int i=-1;i<HEIGHT;i++){
		for(int j=-1;j<WIDTH;j++){
						mvchgat(y+i,x+j,1,A_NORMAL,1,NULL);
		}
	}
}

void color_cursor(int y, int x){
	for(int i=-1;i<HEIGHT;i++){
		for(int j=-1;j<WIDTH;j++){
					if(!((i==-1 && j==-1) || (i==-1 && j==WIDTH-1) || 
								(i==0 && j==0))){
						mvchgat(y+i,x+j,1,A_NORMAL,2,NULL);
					}
		}
	}
}

void refresh_grid(grid g){
	for(int i=0;i<g->h;i++){
		for(int j=0;j<g->w;j++){
			if(g->grid[i][j].state=='r'){
				mvprintw(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET,"%d",
						g->grid[i][j].val);
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET,1,A_NORMAL,
						3+(g->grid[i][j].val),NULL);
				mvchgat(i*HEIGHT+OFFSET-1,j*WIDTH+2+OFFSET-1,3,A_NORMAL,13,
						NULL);
				mvchgat(i*HEIGHT+OFFSET+1,j*WIDTH+2+OFFSET-1,3,A_NORMAL,13,
						NULL);
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET-1,1,A_NORMAL,13,
						NULL);
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET+1,1,A_NORMAL,13,
						NULL);

					}
			else if(g->grid[i][j].state=='c'){
				mvprintw(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET," ");
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET-1,3,A_NORMAL,12,NULL);
				mvchgat(i*HEIGHT+OFFSET+1,j*WIDTH+2+OFFSET-1,3,A_NORMAL,12,
						NULL);
				mvchgat(i*HEIGHT+OFFSET-1,j*WIDTH+2+OFFSET-1,3,A_NORMAL,12,
						NULL);
			}
			else if(g->grid[i][j].state=='d'){
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET-1,1,A_NORMAL,14,NULL);
				mvchgat(i*HEIGHT+OFFSET+1,j*WIDTH+2+OFFSET-1,3,A_NORMAL,12,
						NULL);
				mvchgat(i*HEIGHT+OFFSET-1,j*WIDTH+2+OFFSET-1,3,A_NORMAL,12,
						NULL);
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET+1,1,A_NORMAL,14,NULL);
				mvprintw(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET,"!");
				mvchgat(i*HEIGHT+OFFSET,j*WIDTH+2+OFFSET,1,A_NORMAL,14,NULL);

				}
			}
		}
}

void game(grid g){
		start_color();
		colors_init();
		int x=OFFSET+1;
		int y=OFFSET-1;
		int x2=x;
		int y2=y;
		int posX=0;
		int posY=0;
		int quit=0;
		move(y,x);
		int ch;
		int lost=0;
		random_hit(g);
		while(!quit && !lost){
			color_cursor(y,x);
			refresh_grid(g);
			ch = getch();
			switch(ch){
				case KEY_DOWN:
						if(posY<g->h-1){
							y2+=HEIGHT;
							posY+=1;
						}
						break;
				case KEY_UP:
						if(posY>0){
								y2-=HEIGHT;
								posY-=1;
						}
						break;
				case KEY_LEFT:
						if(posX>0){
								x2-=WIDTH;
								posX-=1;
						}
						break;
				case KEY_RIGHT:
						if(posX<g->w-1){
								x2+=WIDTH;
								posX+=1;
						}
						break;
				case '\n':
						if(g->grid[posY][posX].state=='c'){
							step(g,posY,posX,&lost);
						}
						break;

				case ' ':
						if(g->grid[posY][posX].state=='c'){
							g->grid[posY][posX].state='d';
						}
						else if(g->grid[posY][posX].state=='d'){
							g->grid[posY][posX].state='c';
						}
						break;

				case 'q':
						quit=1;
						break;
			}
			color_default(y,x);
			move(x2,y2);
			x=x2;
			y=y2;
			color_cursor(y,x);
		}
		if(lost){
			clear();
			mvprintw(OFFSET,OFFSET,"That was a bomb");
		}
}

void step(grid g,int posY, int posX,int* lost){
		if(posY<0 || posY>=g->h || posX<0 || posX>=g->w){
				return;
		}
		if(g->grid[posY][posX].val==-1){
				*lost=1;
				return;
		}
		if(g->grid[posY][posX].state=='c'){
				if(g->grid[posY][posX].val==0){
						g->grid[posY][posX].state='r';
						for(int i=-1;i<2;i++){
								for(int j=-1;j<2;j++){
										if(i!=posX || j!=posY){
												step(g,posY+i,posX+j,lost);
										}
								}
						}
				}
				else{
						g->grid[posY][posX].state='r';
				}
				return;
		}
		else{
				return;
		}
}

		

										



