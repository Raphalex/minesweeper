#include "grid.h"
#include "io.h"

int main(int argc, char ** argv){
	if(argc<4){
		printf("Usage : %s <height> <width> <difficulty (1-9)>\n",argv[0]);
		exit (1);
	}
	srand(time(NULL));
	int height = atoi(argv[1]);
	int width = atoi(argv[2]);
	int diff = atoi(argv[3]);
	grid g=grid_init(height,width,diff);
	g=attr_nbr(g);
	ncurses_init();
	display_grid(g);
	game(g);
	free_grid(g);
	refresh();
	getch();
	endwin();
	return 0;
}

